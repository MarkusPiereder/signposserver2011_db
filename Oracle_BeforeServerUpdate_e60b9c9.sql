-- This Script needs to be executed on Oracle signpos Server installations before server commit e60b9c9 !
-- Drop FK constraints because hash name of FK_ changed (NHIBERNATE compares by constraint name and not by table name and column names)
alter table TBLADDRESS drop constraint FK9011D49C1383A25F;
alter table TBLATTACHMENT drop constraint FK4D2C6AEB3D061B11;
alter table TBLATTACHMENT drop constraint FK4D2C6AEB2C48B0D9;
alter table TBLATTACHMENT drop constraint FK4D2C6AEB82C5B19D;
alter table TBLATTACHMENT drop constraint FK4D2C6AEB3620B6B7;
alter table TBLAUDITLOG drop constraint FK1C9E86E78B07AF91;
alter table TBLAUDITLOG drop constraint FK1C9E86E74976B469;
alter table TBLAUDITLOG drop constraint FK1C9E86E73620B6B7;
alter table TBLAUSMARTCARD drop constraint FKAF27407F3620B6B7;
alter table TBLAUSMARTCARD drop constraint FKAF27407F4294B736;
alter table TBLAUSMARTCARD drop constraint FKAF27407F4976B469;
alter table TBLAUSMARTCARDACTION drop constraint FKF35CA26146A1B377;
alter table TBLAUSMARTCARDACTION drop constraint FKF35CA26114F68A34;
alter table TBLAUSMARTCARDACTION drop constraint FKF35CA26144F090F3;
alter table TBLAUSMARTCARDACTIONREASON drop constraint FKD7BF2C3D3620B6B7;
alter table TBLAUSMARTCARDLOG drop constraint FKB243A3DF46A1B377;
alter table TBLAUSMARTCARDPARAMETER drop constraint FKCA804A883620B6B7;
alter table TBLAUSMARTCARDPARAMETER drop constraint FKCA804A8846A1B377;
alter table TBLCERTIFICATE drop constraint FK7D776ED75531C534;
alter table TBLCERTIFICATE drop constraint FK7D776ED73620B6B7;
alter table TBLCERTIFICATE drop constraint FK7D776ED78EC42C7A;
alter table TBLCERTIFICATEVERSION drop constraint FK634A2B957F0499BB;
alter table TBLCLIENT drop constraint FK8B3779CF3620B6B7;
alter table TBLCONNECTIONLOG drop constraint FKADA10E924976B469;
alter table TBLCONNECTIONLOG drop constraint FKADA10E924C7FF19B;
alter table TBLCONNECTIONLOG drop constraint FKADA10E928B07AF91;
alter table TBLCONNECTIONLOG drop constraint FKADA10E92EAA3FBEB;
alter table TBLCRMCONTRACT drop constraint FKDBFEBFD62C48B0D9;
alter table TBLCRMCONTRACT drop constraint FKDBFEBFD6334383A1;
alter table TBLCRMCONTRACT drop constraint FKDBFEBFD63620B6B7;
alter table TBLCRMCUSTOMER drop constraint FK8B0054885737CC19;
alter table TBLCRMCUSTOMER drop constraint FK8B0054883620B6B7;
alter table TBLCRMCUSTOMERCATEGORY drop constraint FK3D6B8D03620B6B7;
alter table TBLCRMINTERACTION drop constraint FKDA3DCBC2C48B0D9;
alter table TBLCRMINTERACTION drop constraint FKDA3DCBC44F090F3;
alter table TBLCRMINTERACTION drop constraint FKDA3DCBC65595A7F;
alter table TBLCRMINTERACTION drop constraint FKDA3DCBC6218E533;
alter table TBLCRMINTERACTION drop constraint FKDA3DCBCF7F46BAF;
alter table TBLCRMINTERACTIONCATEGORY drop constraint FK7B89BDB83620B6B7;
alter table TBLCRMINTERACTIONCATEGORY drop constraint FK7B89BDB865595A7F;
alter table TBLCRMINTERACTIONMETHOD drop constraint FK3DD867F33620B6B7;
alter table TBLCRMINTERACTIONTYPE drop constraint FK5D5566643620B6B7;
alter table TBLCRMINVOICEADDRESS drop constraint FK3EBC18913620B6B7;
alter table TBLCRMINVOICEADDRESS drop constraint FK3EBC18912C48B0D9;
alter table TBLCRMSETTINGS drop constraint FK82D2BDBF828BF931;
alter table TBLCRMSETTINGS drop constraint FK82D2BDBF5EBC5F5E;
alter table TBLCRMSETTINGS drop constraint FK82D2BDBF3620B6B7;
alter table TBLCRMSETTINGS drop constraint FK82D2BDBF99425DF4;
alter table TBLCRMSETTINGS drop constraint FK82D2BDBFC8377E27;
alter table TBLCRMTASK drop constraint FKFC86D8771F6E90FC;
alter table TBLCRMTASK drop constraint FKFC86D87744F090F3;
alter table TBLCRMTASK drop constraint FKFC86D8777B16109;
alter table TBLCRMTASK drop constraint FKFC86D8772C48B0D9;
alter table TBLCRMTASK drop constraint FKFC86D877B8F426E2;
alter table TBLCRMTASK_GROUP drop constraint FKD4DEC3373D061B11;
alter table TBLCRMTASK_GROUP drop constraint FKD4DEC337DC4C0487;
alter table TBLCRMTASK_INTERACTION drop constraint FKC5EBE4AE3D061B11;
alter table TBLCRMTASK_INTERACTION drop constraint FKC5EBE4AE82C5B19D;
alter table TBLCRMTASKTYPE drop constraint FKDC5224C73620B6B7;
alter table TBLDISPLAYLAYOUT drop constraint FKBD09FB405E759B6B;
alter table TBLDISPLAYLAYOUT drop constraint FKBD09FB40105110EF;
alter table TBLDISPLAYLAYOUT drop constraint FKBD09FB4018AE8A23;
alter table TBLDISPLAYLAYOUTELEMENT drop constraint FKC0C61EA41274C69B;
alter table TBLDISPLAYLAYOUTPAGE drop constraint FK15757139EC500675;
alter table TBLDOCUMENT drop constraint FKA8902B453593B8A2;
alter table TBLDOCUMENT drop constraint FKA8902B4512E2664;
alter table TBLDOCUMENT drop constraint FKA8902B453620B6B7;
alter table TBLDOCUMENT drop constraint FKA8902B452A5B5833;
alter table TBLDOCUMENTTEMPLATE drop constraint FKA752C6B13593B8A2;
alter table TBLDOCUMENTTEMPLATE drop constraint FKA752C6B13620B6B7;
alter table TBLDOCUMENTTEMPLATEVERSION drop constraint FK98B1F9833593B8A2;
alter table TBLDOCUMENTTEMPLATEVERSION drop constraint FK98B1F98358111059;
alter table TBLDOCUMENTTEMPLATEVERSION drop constraint FK98B1F98372E2FAFF;
alter table TBLFORMSLEADSTEP drop constraint FK67D3BD3D2EFA9AB0;
alter table TBLFORMSLEADSTEP drop constraint FK67D3BD3DD25FDC29;
alter table TBLFORMSLEADSTEP_GROUP drop constraint FK8A1BABCDDC4C0487;
alter table TBLFORMSLEADSTEP_GROUP drop constraint FK8A1BABCD89C8F410;
alter table TBLFORMSLEADSTEP_ROLE drop constraint FKF195D03889C8F410;
alter table TBLFORMSLEADSTEP_ROLE drop constraint FKF195D0385FA76ADB;
alter table TBLFORMSSTEPAUTHORIZATION drop constraint FK16C8974EEDB2474B;
alter table TBLFORMSSTEPAUTHORIZATION drop constraint FK16C8974EA3E81F77;
alter table TBLGROUP drop constraint FKA74E3A953620B6B7;
alter table TBLGROUP_ROLE drop constraint FKA2FF334DC4C0487;
alter table TBLGROUP_ROLE drop constraint FKA2FF3345FA76ADB;
alter table TBLGSABORTREASON drop constraint FKA996D64C3620B6B7;
alter table TBLGSEVENT drop constraint FKCD64685A3620B6B7;
alter table TBLGSEVENT drop constraint FKCD64685AD9EF9841;
alter table TBLGSEVENT_ABORTREASON drop constraint FKE73E8D337082429;
alter table TBLGSEVENT_ABORTREASON drop constraint FKE73E8D3370B6BFE9;
alter table TBLGSEVENT_GROUP drop constraint FK8BDC9D0A70B6BFE9;
alter table TBLGSEVENT_GROUP drop constraint FK8BDC9D0ADC4C0487;
alter table TBLGSEVENT_ROLE drop constraint FKBDE2009570B6BFE9;
alter table TBLGSEVENT_ROLE drop constraint FKBDE200955FA76ADB;
alter table TBLGSEVENT_SAC drop constraint FKC213C7BC8D01FD15;
alter table TBLGSEVENT_SAC drop constraint FKC213C7BC70B6BFE9;
alter table TBLGSFIELDDEFINITION drop constraint FK60A2D37569E976AA;
alter table TBLGSFIELDDEFINITION drop constraint FK60A2D3752EFA9AB0;
alter table TBLGSFIELDDEFINITION drop constraint FK60A2D3753620B6B7;
alter table TBLGSFIELDDEFINITION drop constraint FK60A2D3758FF54B93;
alter table TBLGSFIELDDEFINITIONLISTENTRY drop constraint FKDE8AD0DF3620B6B7;
alter table TBLGSFIELDDEFINITIONLISTENTRY drop constraint FKDE8AD0DFDD524F7C;
alter table TBLGSFIELDSET drop constraint FK8BEB06D6966C1F5D;
alter table TBLGSFIELDSET drop constraint FK8BEB06D69EA47F5A;
alter table TBLGSFIELDSET drop constraint FK8BEB06D63620B6B7;
alter table TBLGSFIELDSETDATACOLUMN drop constraint FKE455A6A869E976AA;
alter table TBLGSFIELDSETDATAROW drop constraint FK8CB540D669E976AA;
alter table TBLGSLEAD drop constraint FK5DDF08302EFA9AB0;
alter table TBLGSLEAD drop constraint FK5DDF08309C8F9F7F;
alter table TBLGSLEAD drop constraint FK5DDF0830B61B434A;
alter table TBLGSLEAD drop constraint FK5DDF083094D56A3A;
alter table TBLGSLEAD drop constraint FK5DDF08304032CF3D;
alter table TBLGSLEAD drop constraint FK5DDF08303620B6B7;
alter table TBLGSLEADDATA drop constraint FKB5756E80CD02264B;
alter table TBLGSLEADDATA drop constraint FKB5756E80444A7E78;
alter table TBLGSLEADDATA drop constraint FKB5756E80DD524F7C;
alter table TBLGSLEADDEFINITION drop constraint FK76C1EDAF966C1F5D;
alter table TBLGSLEADDEFINITION drop constraint FK76C1EDAF9EA47F5A;
alter table TBLGSLEADDEFINITION drop constraint FK76C1EDAF3620B6B7;
alter table TBLGSLEADDEFINITIONFIELD drop constraint FKD4DCB759DD524F7C;
alter table TBLGSLEADDEFINITIONFIELD drop constraint FKD4DCB75924EA4040;
alter table TBLGSLEADDEFINITIONFIELD drop constraint FKD4DCB759BB94C815;
alter table TBLGSLEADDEFINITIONFIELD drop constraint FKD4DCB7593620B6B7;
alter table TBLGSLEADDEFINITIONFIELD drop constraint FKD4DCB759EDD20BB;
alter table TBLGSLEADDEFINITION_LEADSTATUS drop constraint FKF434A03A2EFA9AB0;
alter table TBLGSLEADDEFINITION_LEADSTATUS drop constraint FKF434A03A6BFB857A;
alter table TBLGSLEADDEFINITIONPAGE drop constraint FK85EDA90A2EFA9AB0;
alter table TBLGSLEADDEFINITIONPAGE drop constraint FK85EDA90A89C8F410;
alter table TBLGSLEADDEFINITIONPAGE drop constraint FK85EDA90A3620B6B7;
alter table TBLGSLEADDOCUMENT drop constraint FKC57903D1444A7E78;
alter table TBLGSLEADDOCUMENT drop constraint FKC57903D189C8F410;
alter table TBLGSLEAD_LEADSTEP drop constraint FKF80357C789C8F410;
alter table TBLGSLEAD_LEADSTEP drop constraint FKF80357C7D7C1A328;
alter table TBLGSLEAD_LEADSTEP drop constraint FKF80357C7444A7E78;
alter table TBLGSLEADSTATUS drop constraint FKC72CB2CE3620B6B7;
alter table TBLGSLEAD_ZDVDOCSHELFDOCUMENT drop constraint FK8088952CA9CF8D2D;
alter table TBLGSLEAD_ZDVDOCSHELFDOCUMENT drop constraint FK8088952C444A7E78;
alter table TBLGSMAILRECIPIENT drop constraint FK6C07C00C68D42E3A;
alter table TBLGSMAILRECIPIENT drop constraint FK6C07C00C3620B6B7;
alter table TBLGSMAILTEMPLATE drop constraint FK2CCA22E86F00C153;
alter table TBLGSMAILTEMPLATE drop constraint FK8B098FCB3620B6B7;
alter table TBLGSMAILTEMPLATE drop constraint FK8B098FCB89C8F410;
alter table TBLGSMAILTEMPLATE drop constraint FK8B098FCBF715B4EC;
alter table TBLGSMT_FD_PLACEHOLDER drop constraint FKF06067B4DD524F7C;
alter table TBLGSMT_FD_PLACEHOLDER drop constraint FKF06067B468D42E3A;
alter table TBLGSMT_FIELDDEFINITION drop constraint FKCE94771F68D42E3A;
alter table TBLGSMT_FIELDDEFINITION drop constraint FKCE94771FDD524F7C;
alter table TBLGSPDFGENERATIONSETTINGS drop constraint FKA5072A2F3620B6B7;
alter table TBLGSPDFGENERATIONSETTINGS drop constraint FKA5072A2F2EFA9AB0;
alter table TBLGSSALESACTION drop constraint FK73EA0AD82EFA9AB0;
alter table TBLGSSALESACTION drop constraint FK73EA0AD83620B6B7;
alter table TBLGSSALESACTION drop constraint FK73EA0AD8D9EF9841;
alter table TBLGSSALESACTIONCONTAINER drop constraint FKC506F4DB3620B6B7;
alter table TBLGSSALESACTIONCONTAINER drop constraint FKC506F4DBD9EF9841;
alter table TBLGSSALESACTIONCONTENT drop constraint FK531BFC1DE0D06C69;
alter table TBLGSSALESACTIONCONTENT drop constraint FK531BFC1D3620B6B7;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82B2C48B0D9;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82BE0D06C69;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82B3620B6B7;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82B70B6BFE9;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82B4976B469;
alter table TBLGSSALESACTIONPROCESS drop constraint FKD438E82B444A7E78;
alter table TBLGSSALESACTION_SAC drop constraint FKDA10B666E0D06C69;
alter table TBLGSSALESACTION_SAC drop constraint FKDA10B6668D01FD15;
alter table TBLGSSAP_ABORTREASON drop constraint FKF4C2F5C17082429;
alter table TBLGSSAP_ABORTREASON drop constraint FKF4C2F5C1644EE385;
alter table TBLGSSHORTMESSAGERECIPIENT drop constraint FK3C6B5F32ACB9ADF8;
alter table TBLGSSHORTMESSAGERECIPIENT drop constraint FK3C6B5F323620B6B7;
alter table TBLGSSHORTMESSAGETEMPLATE drop constraint FK3B80A4B789C8F410;
alter table TBLGSSHORTMESSAGETEMPLATE drop constraint FK3B80A4B7F715B4EC;
alter table TBLGSSHORTMESSAGETEMPLATE drop constraint FK3B80A4B73620B6B7;
alter table TBLGSSHORTMESSAGETEMPLATE drop constraint FK2A74C27A6F00C153;
alter table TBLGSSMT_FD_PLACEHOLDER drop constraint FK11B31965DD524F7C;
alter table TBLGSSMT_FD_PLACEHOLDER drop constraint FK11B31965ACB9ADF8;
alter table TBLGSSMT_FIELDDEFINITION drop constraint FKE36F6178ACB9ADF8;
alter table TBLGSSMT_FIELDDEFINITION drop constraint FKE36F6178DD524F7C;
alter table TBLGSTHUMBNAIL drop constraint FK968CD5523620B6B7;
alter table TBLHARDWARE drop constraint FK32BCDF14688DAC17;
alter table TBLHARDWARE drop constraint FK32BCDF14239ADF69;
alter table TBLLAYOUTSPECIFICATION drop constraint FK40495EFD3620B6B7;
alter table TBLLAYOUTSPECIFICATIONFIELD drop constraint FK344169078B756F2D;
alter table TBLPAGECONFIGURATION drop constraint FK342F98E12A5B5833;
alter table TBLPAGECONFIGURATION drop constraint FK342F98E12EFA9AB0;
alter table TBLPARAMETER drop constraint FKB96CE6033AA4675;
alter table TBLPARAMETER drop constraint FKB96CE6031C952022;
alter table TBLPARAMETER drop constraint FKB96CE6032C48B0D9;
alter table TBLPARAMETER drop constraint FKB96CE6039C1D8F81;
alter table TBLPARAMETER drop constraint FKB96CE603334383A1;
alter table TBLPARAMETER drop constraint FKB96CE6034C5803D5;
alter table TBLPARAMETER drop constraint FKB96CE6038FF54B93;
alter table TBLPARAMETER drop constraint FKB96CE6032C3715FC;
alter table TBLPARAMETER drop constraint FKB96CE603C96CEBF8;
alter table TBLPARAMETER drop constraint FKB96CE603A9CF8D2D;
alter table TBLPARAMETER drop constraint FKB96CE6038C877152;
alter table TBLPARAMETER drop constraint FKB96CE603ABBDD36D;
alter table TBLPARAMETER drop constraint FKB96CE6038B07AF91;
alter table TBLPARAMETER drop constraint FKB96CE6032A5B5833;
alter table TBLPARAMETER drop constraint FKDD9636274A960216;
alter table TBLPARAMETER drop constraint FKDD9636275D0D7B24;
alter table TBLPARAMETER drop constraint FKB96CE6033620B6B7;
alter table TBLPLUGINASSIGNMENT drop constraint FKA2FDFF8E1E3AB3B8;
alter table TBLPLUGINASSIGNMENT drop constraint FKA2FDFF8E3620B6B7;
alter table TBLPLUGININSTANCE drop constraint FKB20EBC28E11017A;
alter table TBLPLUGINPARAMETERINSTANCE drop constraint FK83815AFF1F82A3AA;
alter table TBLPLUGINPARAMETERINSTANCE drop constraint FK83815AFF9455333;
alter table TBLPLUGINPARAMETERTYPE drop constraint FK50EDDC92C8ECC69D;
alter table TBLPOSIDENTABORTREASON drop constraint FKC9A664C7F0FDA88C;
alter table TBLPOSIDENTADDITIDENTMETHOD drop constraint FKB754A62BF0FDA88C;
alter table TBLPOSIDENTDOCUMENTTYPE drop constraint FK923D58A8F0FDA88C;
alter table TBLPOSIDENTIDENTCONFIG drop constraint FKF42BD018F0FDA88C;
alter table TBLPOSIDENTIDENTCONFIG drop constraint FKF42BD018CCAAC6F1;
alter table TBLPOSIDENTIDENTCONFIGABORTREA drop constraint FK604660E1EC37303B;
alter table TBLPOSIDENTIDENTCONFIGABORTREA drop constraint FK604660E1D76A2C23;
alter table TBLPOSIDENTIDENTCONFIGADDIIDEN drop constraint FK596D7093A62EFABA;
alter table TBLPOSIDENTIDENTCONFIGADDIIDEN drop constraint FK596D7093EC37303B;
alter table TBLPOSIDENTIDENTCONFIG_GROUP drop constraint FK8DB0C718E9390763;
alter table TBLPOSIDENTIDENTCONFIG_GROUP drop constraint FK8DB0C718EC37303B;
alter table TBLPOSIDENTIDENTCONFIG_ROLE drop constraint FKFEAD7D554056C1B2;
alter table TBLPOSIDENTIDENTCONFIG_ROLE drop constraint FKFEAD7D55EC37303B;
alter table TBLPOSIDENTIDENTCONFIGTASKLIST drop constraint FK9AA9383EEC37303B;
alter table TBLPOSIDENTIDENTCONFIGTASKLIST drop constraint FK9AA9383EE2BCE617;
alter table TBLPOSIDENTIDENTCONFIGTASKLIST drop constraint FK9AA9383E497E7E6B;
alter table TBLPOSIDENTIDENTIFICATION drop constraint FK9E2E548FCBF459B9;
alter table TBLPOSIDENTIDENTIFICATION drop constraint FK9E2E548FE2BCE617;
alter table TBLPOSIDENTIDENTIFICATION drop constraint FK9E2E548FEC37303B;
alter table TBLPOSIDENTIDENTIFICATION drop constraint FK9E2E548FDC5BF748;
alter table TBLPOSIDENTIDENTIFICATIONDATA drop constraint FKEC5E839E4A960216;
alter table TBLPOSIDENTIDENTIFICATIONLOG drop constraint FK7E7269F24A960216;
alter table TBLPOSIDENTIDENTRECORD drop constraint FKC1E5CFDB4A960216;
alter table TBLPOSIDENTIDENTSABORTREASONS drop constraint FK849C132AD76A2C23;
alter table TBLPOSIDENTIDENTSABORTREASONS drop constraint FK849C132A4A960216;
alter table TBLPOSIDENTTASKLIST drop constraint FK435CACB3F0FDA88C;
alter table TBLPRODUCT drop constraint FKB4D069A5239ADF69;
alter table TBLPRODUCTCONFIGURATION drop constraint FKFB870B754C7FF19B;
alter table TBLPRODUCTCONFIGURATION drop constraint FKFB870B753620B6B7;
alter table TBLPRODUCTCONFIGURATION drop constraint FKFB870B75EAA3FBEB;
alter table TBLPRODUCTVERSION drop constraint FK9F10236BEAA3FBEB;
alter table TBLREPORT drop constraint FKAA9BBD73F0FDA88C;
alter table TBLREPORT drop constraint FK46305D63593B8A2;
alter table TBLREPORT_GROUP drop constraint FK890465E69D60A18F;
alter table TBLREPORT_GROUP drop constraint FK890465E6DC4C0487;
alter table TBLREPORTPROPERTY drop constraint FK326492279D60A18F;
alter table TBLREPORT_ROLE drop constraint FK2E091CC79D60A18F;
alter table TBLREPORT_ROLE drop constraint FK2E091CC75FA76ADB;
alter table TBLROLE drop constraint FKC2C3C4343620B6B7;
alter table TBLROLE_RIGHT drop constraint FK87CD16595FA76ADB;
alter table TBLROLE_RIGHT drop constraint FK87CD16599FE713BB;
alter table TBLSCSCANNEDDOCUMENT drop constraint FK10267CB53620B6B7;
alter table TBLSIGNATURECONFIGURATION drop constraint FK519039C437AC7C7F;
alter table TBLSIGNATURECONFIGURATION drop constraint FK519039C418AE8A23;
alter table TBLSIGNATURECONFIGURATION drop constraint FKB6AFEB2AA4C2AD55;
alter table TBLSIGNATUREFLOW drop constraint FK775B3E92E2C8BEC2;
alter table TBLSIGNATUREFLOW drop constraint FK775B3E92F1F7476F;
alter table TBLSIGNATUREFLOW drop constraint FK775B3E92ABBDD36D;
alter table TBLSIGNATUREFLOW drop constraint FK775B3E92465538B1;
alter table TBLSIGNATUREREQUEST drop constraint FK80FC12B312E2664;
alter table TBLSIGNATUREREQUEST drop constraint FK80FC12B327C4FA1E;
alter table TBLSIGNATUREREQUEST drop constraint FK80FC12B34976B469;
alter table TBLSIGNFLOWATTACHMENT drop constraint FK26B14318C96CEBF8;
alter table TBLSIGNFLOWATTACHMENTDATA drop constraint FKA807AD6827C4FA1E;
alter table TBLSIGNFLOWATTACHMENTFILE drop constraint FK2AA0F85A99BEA50E;
alter table TBLSIGNFLOWATTACHMENTFILE drop constraint FK2AA0F85A8A171BEE;
alter table TBLSIGNFLOWCHECKBOXCONFIG drop constraint FKB6504EBF22A7E8A8;
alter table TBLSIGNFLOWDOCUMENT drop constraint FK29C0E12027C4FA1E;
alter table TBLSIGNFLOWDOCUMENT drop constraint FK29C0E1203620B6B7;
alter table TBLSIGNFLOWLOG drop constraint FK1D780BE327C4FA1E;
alter table TBLSIGNFLOWNOTIFICATION drop constraint FK9126AD9EF7775FD2;
alter table TBLSIGNFLOWNOTIFICATION drop constraint FK9126AD9E3620B6B7;
alter table TBLSIGNFLOWSHAREDFOLDER drop constraint FK632DC8103620B6B7;
alter table TBLSIGNFLOWSHAREDFOLDER_GROUP drop constraint FKFD4C2840DC4C0487;
alter table TBLSIGNFLOWSHAREDFOLDER_GROUP drop constraint FKFD4C2840F65570D8;
alter table TBLSIGNFLOWSHAREDFOLDER_USER drop constraint FKAB6D34024976B469;
alter table TBLSIGNFLOWSHAREDFOLDER_USER drop constraint FKAB6D3402F65570D8;
alter table TBLSIGNFLOWTYPE drop constraint FKE8B22333620B6B7;
alter table TBLSIGREQUEST_SIGCONFIGURATION drop constraint FK2C3342A8FF54B93;
alter table TBLSIGREQUEST_SIGCONFIGURATION drop constraint FK2C3342AC96CEBF8;
alter table TBLSTYLECONFIGURATION drop constraint FKEC15161D3620B6B7;
alter table TBLTENANT drop constraint FK9CE350CC2D2FF9A6;
alter table TBLTENANT drop constraint FK9CE350CC5293EEFF;
alter table TBLTENANT drop constraint FK9CE350CCC0D07637;
alter table TBLTENANT drop constraint FK9CE350CCD25FDC29;
alter table TBLTENANT drop constraint FK9CE350CCC6FF5223;
alter table TBLTENANT drop constraint FKDC881DEDB24C5821;
alter table TBLTENANT drop constraint FKDC881DED372A685E;
alter table TBLTENANT drop constraint FKDC881DED3B54C2D7;
alter table TBLTRANSLATIONLANGUAGE drop constraint FK404BC65FAD83D72C;
alter table TBLTRANSLATIONSTRING drop constraint FKC572EECA7F1F3230;
alter table TBLUSER drop constraint FKB0BB8DCF3620B6B7;
alter table TBLUSER drop constraint FKB0BB8DCFD3B17ABF;
alter table TBLUSERAUTHENTICATIONTOKEN drop constraint FK9D3F800C4976B469;
alter table TBLUSER_GROUP drop constraint FK6FCB820FDC4C0487;
alter table TBLUSER_GROUP drop constraint FK6FCB820F4976B469;
alter table TBLUSER_RIGHT drop constraint FKE1272C2A4976B469;
alter table TBLUSER_RIGHT drop constraint FKE1272C2A9FE713BB;
alter table TBLWORKFLOWIMPLEMENTATION drop constraint FK94A8E5973620B6B7;
alter table TBLWORKFLOWIMPLEMENTATION drop constraint FK94A8E59718718D8C;
alter table TBLWORKFLOWIMPLEMENTATION drop constraint FK94A8E597966C1F5D;
alter table TBLZDVDOCSHELF drop constraint FKD4FA919E3620B6B7;
alter table TBLZDVDOCSHELF drop constraint FKD4FA919EC9309E58;
alter table TBLZDVDOCSHELFDOCUMENT drop constraint FK3B41C2B9D9EF9841;
alter table TBLZDVDOCSHELFDOCUMENT drop constraint FK3B41C2B99D19E1C4;
alter table TBLZDVDOCSHELFDOCUMENT_USER drop constraint FK777102EBA9CF8D2D;
alter table TBLZDVDOCSHELFDOCUMENT_USER drop constraint FK777102EB4976B469;
alter table TBLZDVDOCSHELF_DSDOCUMENT drop constraint FKE5956F41CE104D01;
alter table TBLZDVDOCSHELF_DSDOCUMENT drop constraint FKE5956F41A9CF8D2D;
alter table TBLZDVDOCSHELF_GROUP drop constraint FK232AD03ECE104D01;
alter table TBLZDVDOCSHELF_GROUP drop constraint FK232AD03EDC4C0487;
alter table TBLZDVDOCSHELF_ROLE drop constraint FK4E9CEB21CE104D01;
alter table TBLZDVDOCSHELF_ROLE drop constraint FK4E9CEB215FA76ADB;
alter table TBLZDVSALESACTION_DOCSHELF drop constraint FKD3B7FD5BCE104D01;
alter table TBLZDVSALESACTION_DOCSHELF drop constraint FKD3B7FD5B55238131;
alter table TBLZDVSALESACTIONDOCSHELF drop constraint FK9250471CE0D06C69;
alter table TBLZDVSALESACTIONDOCSHELF drop constraint FK9250471CCE104D01;

-- Drop Indexes where named changed (NHIBERNATE compares by index name and not by table name and column names)
drop index idx__SignatureConfigurationId;
drop index idx__TenantId;
drop index idx__DocumentId;
drop index idx__SignFlowDocumentId;
drop index idx__Token;
drop index idx__TemporaryToken;
drop index idx__StatusId;
drop index idx__SignatureFlowId;
