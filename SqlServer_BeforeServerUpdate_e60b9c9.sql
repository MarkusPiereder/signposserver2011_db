-- Drop Indexes where named changed (NHIBERNATE compares by index name and not by table name and column names)
drop index tblGSFieldDefinition.idx__SignatureConfigurationId;
drop index tblFileStore.idx__TenantId;
drop index tblParameter.idx__DocumentId;
drop index tblParameter.idx__SignFlowDocumentId;
drop index tblParameter.idx__SignatureConfigurationId;
drop index tblScanRequest.idx__TenantId;
drop index tblUserAuthenticationToken.idx__Token;
drop index tblWorkstationDevice.idx__TemporaryToken;
drop index tblWorkstationDevice.idx__Token;
drop index tblWorkstationDevice.idx__StatusId;
drop index tblSignatureFlow.idx__DocumentId;
drop index tblSignatureRequest.idx__SignatureFlowId;
drop index tblSignatureRequest.idx__SignatureConfigurationId;
drop index tblSignFlowAttachmentData.idx__SignatureFlowId;
drop index tblSignFlowDocument.idx__SignatureFlowId;
drop index tblSignFlowLog.idx__SignatureFlowId;
